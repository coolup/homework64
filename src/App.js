import React from 'react';
import AddTaskForm from './Task/AddTaskForm';
import Task from './Task/Task';
import './App.css'

class App extends React.Component {
  state = {
    currentTask: '',
    task: [
      { desc: 'Buy milk', id: 1 },
      { desc: 'Walk with dog', id: 2 },
      { desc: 'Do homework', id: 3 }
    ],
  };

  AddTaskDesc = (event) => {
    const currentTask = event.target.value;
    this.setState({ currentTask });

  }
  AddTask = () => {
    const task = [...this.state.task];    
    task.unshift({
      desc: this.state.currentTask,
      id: task.length === 0 ? 0 : task[task.length-1].id+1
    });
    this.setState({ task });

  }
  removeTask = (id) => {
    const index = this.state.task.findIndex(t => t.id === id);
    const task = [...this.state.task];
    task.splice(index, 1);
    this.setState({ task });
  }
  render() {
    return (
      <div>
        <AddTaskForm newform={event => this.AddTaskDesc(event)} 
                    add={() => this.AddTask(this.state.task.length)} />
        {
          this.state.task.map((task) => {
            return <Task 
              desc={task.desc} 
              key={task.id} 
              remove={() => this.removeTask(task.id)}>
              </Task>
          })}
      </div>
    );
  }
}

export default App;
