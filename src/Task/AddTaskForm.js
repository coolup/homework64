import React from 'react';
import './task.css';

const AddTaskFrom = (props) => {
    return (
        <div className="taskform">
            <input type="text" placeholder="Add new task" onChange={props.newform}/>
            <input type ="button" value="Add" onClick={props.add}/>
        </div>
    );
}
export default AddTaskFrom;