import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import './task.css';

const Task = (props) => {
    return (
        <div className="task">
            <p>{props.desc}</p>
            <FontAwesomeIcon icon={faTrashAlt} className="icon" onClick={props.remove}/>
        </div>
    );
}
export default Task;